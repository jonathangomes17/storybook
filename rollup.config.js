import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import external from "rollup-plugin-peer-deps-external";
import { terser } from "rollup-plugin-terser";
// import { uglify } from "rollup-plugin-uglify";
// import copy from 'rollup-plugin-copy';

import packageJSON from "./package.json";
const input = "./src/index.js";
const minifyExtension = pathToFile => pathToFile.replace(/\.js$/, ".js");

export default [
   // // Common JS
   // {
   //    input,
   //    output: {
   //       file: minifyExtension(packageJSON.main),
   //       format: "cjs",
   //       sourcemap: true
   //    },
   //    plugins: [
   //       babel({
   //          exclude: "node_modules/**"
   //       }),
   //       external(),
   //       resolve(),
   //       commonjs(),
   //       uglify()
   //    ]
   // },
   // ES
   {
      input,
      output: {
         file: minifyExtension(packageJSON.module),
         format: "es",
         // exports: "named",
         sourcemap: true
      },
      external: ['styled-components'],
      plugins: [
         babel({
            exclude: "node_modules/**"
         }),
         external(),
         resolve(),
         commonjs(),
         terser(),
         // copy({
         //    targets: [
         //       { src: 'src/style/drmanhattan.less', dest: 'dist' },
         //    ]
         // })
      ]
   }
];