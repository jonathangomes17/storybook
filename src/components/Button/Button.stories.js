import React from 'react';
import { action } from '@storybook/addon-actions';
import Button from './Button';

export default {
   title: 'Button',
};

export const basicUsage = () => <Button type="primary" onClick={action('clicked')}> Click Me! </Button>;

export const loading = () => <Button type="primary" onClick={action('clicked')} loading> Click Me With Loading! </Button>;

export const emoji = () => (
   <Button onClick={action('clicked')}>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
   </Button>
);
