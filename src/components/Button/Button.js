import React from "react";
import { Button } from 'antd';
import 'antd/es/button/style/index.css';

const CustomButton = ({children, ...rest}) => {
   return (
      <Button {...rest}>{children}</Button>
   );
};

export default CustomButton;