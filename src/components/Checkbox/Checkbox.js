import React from "react";
import { Checkbox } from 'antd';
import 'antd/es/checkbox/style/index.css';

const CustomCheckbox = ({children, ...rest}) => {
   return (
       <Checkbox {...rest}>{children}</Checkbox>
   );
};

export default CustomCheckbox;