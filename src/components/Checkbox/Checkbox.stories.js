import React, { useState } from 'react';
import Checkbox from './Checkbox';

export default {
   title: 'Checkbox',
};

export const usageBasic = () => {
   const [isChecked, setCheck] = useState(false);

   const handleChangeCheckbox = () => setCheck(prevState => !prevState);

   return (
       <div>
         <p> Valor checkbox: {isChecked.toString()} </p>
         <Checkbox onChange={handleChangeCheckbox}> Basic usage </Checkbox>
       </div>
   )
};

