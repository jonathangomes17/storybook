import React from "react";
import styled from 'styled-components';

const Container = styled.a`
    color: #006ace;
    text-decoration: none;
    background-color: transparent;
    outline: none;
    cursor: pointer;
    transition: color 0.3s ease;
    touch-action: manipulation;
`;

const Link = ({href, value, ...rest}) => {
   return <Container href={href} aria-label={value} {...rest}> {value} </Container>;
};

export default Link;
