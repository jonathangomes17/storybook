import React from 'react';
import Link from './Link';

export default {
   title: 'Links',
};

export const basicUsage = () => {
   return (
       <div>
         <Link href="https://eagle.madeiramadeira.com.br" value="Link padrão" />
         <br/>
         <Link href="https://eagle.madeiramadeira.com.br" value="Links com nova aba em alta performance" rel="noopener noreferrer" target="_blank" />
       </div>
   );
};
