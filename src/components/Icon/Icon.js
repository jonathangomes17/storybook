import React from "react";
import { Icon } from 'antd';
import 'antd/es/icon/style/index.css';

const CustomIcon = (props) => {
   return (
      <Icon {...props} />
   );
};

export default CustomIcon;