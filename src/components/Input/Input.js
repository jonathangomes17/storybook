import React from "react";
import { Input } from 'antd';
import 'antd/es/input/style/index.css';

import styled from 'styled-components';

const StyledInput = styled.div`
   .ant-input {
      border: 1px solid #8e8e8e;
   }
   
   .ant-input::placeholder {
      color: #8e8e8e;
   }
`;

const CustomInput = (props) => {
   return (
      <StyledInput>
         <Input {...props} />
      </StyledInput>
   );
};

CustomInput.Password = Input.Password;

export default CustomInput;