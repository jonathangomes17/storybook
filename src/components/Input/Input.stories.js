import React from 'react';
import Input from './Input';

export default {
   title: 'Input',
};

export const basicUsage = () => <Input placeholder="Basic usage" />;

export const passwordUsage = () => <Input.Password placeholder="Input Password" />;
