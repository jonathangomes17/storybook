import React from "react";
import { Col } from 'antd';
import 'antd/es/grid/style/index.css';

const CustomCol = ({children, ...rest}) => <Col {...rest}>{children}</Col>;

export default CustomCol;