import React from "react";
import { Row } from 'antd';
import 'antd/es/grid/style/index.css';

const CustomRow = ({children, ...rest}) => <Row {...rest}>{children}</Row>;

export default CustomRow;