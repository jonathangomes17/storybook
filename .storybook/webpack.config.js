const path = require('path');

module.exports = async ({ config, mode }) => {
   // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
   // You can change the configuration based on that.
   // 'PRODUCTION' is used when building the static version of storybook.

   // Make whatever fine-grained changes you need
   config.module.rules.push({
      test: /\.less$/,
      loader: 'style-loader',
      include: path.resolve(__dirname, '../')
   });

   config.module.rules.push({
      test: /\.less$/,
      loader: 'css-loader',
      include: path.resolve(__dirname, '../')
   });

   config.module.rules.push({
      test: /\.less$/,
      loader: 'less-loader', // compiles Less to CSS
      options: {
         javascriptEnabled: true
      },
      include: path.resolve(__dirname, '../')
   });

   // Return the altered config
   return config;
};