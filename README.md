# Bulkylog StoryBook
> Design system UI

### Objective
* Proporcionar ao time Bulkylog uma maneira entender o uso dos componentes de apresentação e como consumi-los em seus respectivos produtos. 

### How to create a component
1. Dentro da pasta `src/components` crie uma nova pasta com o nome do componente de apresentação
2. Crie o arquivo `{ComponentName}.js` e `{ComponentName}.stories.js`
> Fonte: [StorybookJs Org](https://storybook.js.org/docs/addons/using-addons/)

### What can not do
*  Encapsulamento de regra de negócio

### Rules for developers
Convenção adotada com o prefixo de branches (Git)
> Deve ser criado com base na master `git checkout -b feature/tela_sso`  
 
 * `feature/` - Nova funcionalidade
 * `bugfix/` - Problemas triviais
 * `hotfix/` - Problemas críticos

[ConventionalCommits](https://www.conventionalcommits.org/pt-br/v1.0.0-beta.4) foi adotada a padronização de commits
> Deve ser criado no início de cada commit `git commit -am 'perf: ajustes na performance do sso'`.
  * `feat:` Nova funcionalidade (MINOR)
  * `fix:` Correção de problemas (PATCH)
  * `refactor:` Refatoração de códigos (PATCH)
  * `perf:` Resolução de problemas de performance (PATCH)
  * `BREAKING CHANGE:` Quebra de versão (MAJOR)
   
[SemVer](https://semver.org/) foi adotado como padronagem de versionamento
> Deve ser respeitado MAJOR.MINOR.PATCH. Ex: 0.1.0

* `MAJOR` - Quebra de versão, deve ser incrementado `(X).0.0`
* `MINOR` - Nova(s) funcionalidade(s), deve ser incrementado `0.(X).0`
* `PATCH` - Correções de problemas, deve ser incrementado `0.0.(X)`

### Contributors
1. [Renata Portela](https://github.com/renataportela)
2. [Jonathan Alves Gomes](https://github.com/jonathangomes17)

